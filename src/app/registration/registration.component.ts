import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/api.service';
import { NgForm } from '@angular/forms';
import { AuthService } from '../shared/auth.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ErrorModel } from '../shared/model/error-model';
import { RegistrationViewModel } from '../shared/viewmodel/registration-view-model';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  private avatar: File;
  private certificate: File;
  error: ErrorModel;

  constructor(private http: HttpClient, private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
  }

  signUp(form: NgForm) {
    const values = form.value;

    this.error = this.validate(values);

    if (this.isErrorsEmpty(this.error)) {
      const payload: FormData = this.constructFormData(values);

      this.http.post('/registration', payload)
        .subscribe(response => {
          this.router.navigate(['/login']);
        }, error => {
          alert('Error while registration');
        });
    }
  }

  private isErrorsEmpty(error: ErrorModel): boolean {
    return Object.keys(error).length === 0;
  }

  handleFileInput({target}) {
    if (target.name === 'avatar') {
      this.avatar = target.files[0];
    } else if (target.name === 'certificate') {
      this.certificate = target.files[0];
    }
  }

  private constructFormData(values: RegistrationViewModel): FormData {
    const payload: FormData = new FormData();

    payload.append('firstName', values.firstName);
    payload.append('lastName', values.lastName);
    payload.append('middleName', values.middleName);
    payload.append('email', values.email);
    payload.append('password', values.password);
    payload.append('confirmPassword', values.confirmPassword);
    payload.append('region', values.region);
    payload.append('city', values.city);
    payload.append('schoolNumber', values.schoolNumber.toString());
    if (this.avatar) {
      payload.append('avatar', this.avatar, this.avatar.name);
    }
    if (this.certificate) {
      payload.append('certificate', this.certificate, this.certificate.name);
    }

    return payload;
  }

  private validate(payload: RegistrationViewModel): ErrorModel {
    const errors: ErrorModel = {};
    if (!payload.firstName) {
      errors.firstName = 'First Name must not be empty';
    }
    if (!payload.lastName) {
      errors.lastName = 'Last Name must not be empty';
    }
    if (!payload.middleName) {
      errors.middleName = 'Middle Name must not be empty';
    }
    if (!payload.password) {
      errors.password = 'Password must not be empty';
    }
    if (payload.password !== payload.confirmPassword) {
      errors.confirmPassword = 'The confirm password does not match with password';
    }
    if (!this.isEmailValid(payload.email)) {
      errors.email = 'Email does not valid';
    }
    if (!payload.region) {
      errors.region = 'Region must not be empty';
    }
    if (!payload.city) {
      errors.city = 'city must not be empty';
    }
    if (!payload.schoolNumber) {
      errors.schoolNumber = 'School Number must not be empty';
    }

    return errors;
  }

  private isEmailValid(email): boolean {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
  }

}

interface FileData {
  name: string;
  file: File;
}
