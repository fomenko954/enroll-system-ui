import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { ResponseViewModel } from './viewmodel/response-view-model';
import { SubjectsViewModel } from './viewmodel/subjects-view-model';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubjectsService {

  constructor(private apiService: ApiService) {
  }

  getAllSubjects(page?: number, size?: number): Observable<ResponseViewModel<Array<SubjectsViewModel>>> {
    let params = new HttpParams();
    let url = '/api/subjects';
    if (!page && !size) {
      url = url + '/all';
    } else {
      params = params.append('page', page.toString());
      params = params.append('size', size.toString());
    }
    return this.apiService.get(url, params);
  }

  deleteSubject(subject: SubjectsViewModel): Observable<ResponseViewModel<Array<string>>> {
    return this.apiService.delete(`/api/subjects/${subject.id}`);
  }

  createSubject(subjectViewModel: SubjectsViewModel): Observable<ResponseViewModel<SubjectsViewModel>> {
    return this.apiService.post('/api/subjects', subjectViewModel, false);
  }

  updateSubject(subjectViewModel: SubjectsViewModel): Observable<ResponseViewModel<SubjectsViewModel>> {
    return this.apiService.put(`/api/subjects/${subjectViewModel.id}`, subjectViewModel);
  }
}
