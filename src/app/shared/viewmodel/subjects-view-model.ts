export interface SubjectsViewModel {
  id?: number;
  name: string;
  isSelected?: boolean;
}
