import { UserRoles } from '../model/user-roles';

export interface UserViewModel {
  id: number;
  firstName: string;
  lastName: string;
  middleName: string;
  role: UserRoles;
  region: string;
  city: string;
  schoolNumber: number;
  certificateUrl: string;
  email: string;
  avatarUrl: string;
  isBlocked: boolean;
  verifyStatus: string;
  userSubjects: Array<any>;
}
