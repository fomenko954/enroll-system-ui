import { SubjectsViewModel } from './subjects-view-model';

export interface FacultyViewModel {
  id: number;
  name: string;
  fundedPlaceCount: number;
  allPlaceCount: number;
  subjects: Array<SubjectsViewModel>;
}
