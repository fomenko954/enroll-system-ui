import { UserViewModel } from './user-view-model';
import { FacultyViewModel } from './faculty-view-model';

export interface ComplaintsViewModel {
  user: UserViewModel;
  faculty: FacultyViewModel;
  status: string;
  expiredDate: Date;
  acceptedDate: Date;
  summaryMarks: number;
}
