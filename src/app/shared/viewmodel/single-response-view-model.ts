import { UserViewModel } from './user-view-model';

export interface SingleResponseViewModel<T> {
  user: UserViewModel;
  data: Array<T>;
}
