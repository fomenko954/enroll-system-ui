export interface ResponseViewModel<D> {
  content: D;
  currentPage: number;
  totalPage: number;
  size: number;
}
