export interface RegistrationViewModel {
  firstName: string;
  lastName: string;
  middleName: string;
  email: string;
  password: string;
  confirmPassword: string;
  city: string;
  region: string;
  schoolNumber: number;
  avatar: File;
  certificate: File;
}
