import { UserAuthResponse } from '../model/user-auth-response';

export interface AuthResponseViewModel {
  token: string;
  user: UserAuthResponse;
}
