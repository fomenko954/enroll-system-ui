import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { URLSearchParams } from 'url';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient,
              private auth: AuthService) {
  }

  get(url: string, params?: HttpParams): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.auth.getToken()}`,
        'Access-Control-Allow-Origin': '*'
      }),
      params
    };
    return this.http.get(url, httpOptions);
  }

  post(url: string, body: any | FormData, canGuest: boolean, contentType?: string): Observable<any> {
    let httpHeaders: HttpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.append('Content-Type', contentType || 'application/json');
    httpHeaders = httpHeaders.append('Accept', 'application/json');
    if (!canGuest) {
      httpHeaders = httpHeaders.append('Authorization', `Bearer ${this.auth.getToken()}`);
    }
    return this.http.post(url, body, {headers: httpHeaders});
  }

  put(url: string, body?: any, params?: HttpParams): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.auth.getToken()}`,
        'Access-Control-Allow-Origin': '*'
      }),
      params
    };
    return this.http.put(url, body, httpOptions);
  }

  delete(url: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.auth.getToken()}`,
        'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.delete(url, httpOptions);
  }
}
