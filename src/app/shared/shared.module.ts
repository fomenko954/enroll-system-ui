import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
import { PageNavComponent } from './components/page-nav/page-nav.component';



@NgModule({
  declarations: [
    SidebarComponent,
    PageNavComponent
  ],
  exports: [
    SidebarComponent,
    PageNavComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class SharedModule { }
