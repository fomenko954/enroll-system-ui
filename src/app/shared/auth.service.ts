import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, ReplaySubject } from 'rxjs';
import { UserViewModel } from './viewmodel/user-view-model';
import { ResponseViewModel } from './viewmodel/response-view-model';
import { AuthViewModel } from './viewmodel/auth-view-model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthResponseViewModel } from './viewmodel/auth-response-view-model';
import { UserAuthResponse } from './model/user-auth-response';
import { UserRoles } from './model/user-roles';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private tokenKey: string = 'enroll-jwt-token';
  private userKey: string = 'authUser';

  private user = new ReplaySubject<UserViewModel>(1);

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  getUser(): Observable<UserAuthResponse | UserViewModel> {
    return this.user;
  }

  setToken(token: string) {
    localStorage.setItem(this.tokenKey, token);
  }

  getToken(): string {
    return localStorage.getItem(this.tokenKey);
  }

  isLoggedIn() {
    return localStorage.getItem(this.tokenKey) !== null;
  }

  logout() {
    localStorage.removeItem(this.tokenKey);
    this.router.navigate(['/login']);
  }

  authenticate(payload: AuthViewModel) {
    const httpHeaders: HttpHeaders = new HttpHeaders();
    httpHeaders.append('Content-Type', 'application/json');
    httpHeaders.append('Accept', 'application/json');
    this.http.post<AuthResponseViewModel>('/authenticate', payload, {headers: httpHeaders})
      .subscribe(result => {
        this.setToken(result.token);
        this.getUserInfo();
        this.setUserInSession(result.user);
        this.redirect(result.user);
      }, error => {
        alert('Error while authorizing');
      });
  }

  getUserInfo(): Observable<any> {
    console.log('get info');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.getToken()}`
      })
    };

    const user = new ReplaySubject<UserViewModel>();
    this.http.get<ResponseViewModel<UserViewModel>>('/profile', httpOptions).subscribe(
      res => {
        user.next(res.content);
        this.user.next(res.content);
        this.setUserInSession(this.convertUserToUserAuth(res.content));
        console.log('set users');
      },
      error => {
        console.log(error);
      });
    return user;
  }

  redirect(user: UserAuthResponse): void {
    switch (user.role) {
      case UserRoles.admin:
        this.router.navigate(['/admin']);
        break;
      case UserRoles.student:
        this.router.navigate(['/students']);
        break;
    }
  }

  hasRole(userRole: UserRoles) {
    const user: UserAuthResponse = this.getUserFromSession();
    return user.role === userRole;
  }

  private setUserInSession(user: UserAuthResponse): void {
    sessionStorage.setItem(this.userKey, JSON.stringify(user));
  }

  private getUserFromSession(): UserAuthResponse {
    return JSON.parse(sessionStorage.getItem(this.userKey));
  }

  private convertUserToUserAuth(user: UserViewModel): UserAuthResponse {
    return {
      firstName: user.firstName,
      lastName: user.lastName,
      middleName: user.middleName,
      role: user.role,
      email: user.email
    };
  }
}
