import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { ComplaintsViewModel } from './viewmodel/complaints-view-model';
import { HttpParams } from '@angular/common/http';
import { ResponseViewModel } from './viewmodel/response-view-model';
import { UserViewModel } from './viewmodel/user-view-model';
import { Status } from './model/status';
import { ComplaintModel } from './model/complaint-model';
import { SingleResponseViewModel } from './viewmodel/single-response-view-model';
import { FacultyViewModel } from './viewmodel/faculty-view-model';

@Injectable({
  providedIn: 'root'
})
export class ComplaintsService {

  constructor(private apiService: ApiService) {
  }

  getAllComplaints(page: number, size: number): Observable<ResponseViewModel<Array<ComplaintsViewModel>>> {
    const params = new HttpParams()
      .append('page', page.toString())
      .append('size', size.toString());
    return this.apiService.get('/api/complaints', params);
  }

  changeStatus(complaint: ComplaintsViewModel, status: Status):
    Observable<ResponseViewModel<SingleResponseViewModel<ComplaintsViewModel>>> {
    const body: ComplaintModel = {
      userId: complaint.user.id,
      facultyId: complaint.faculty.id,
      status
    };
    return this.apiService.put('/api/complaints', body);
  }
}
