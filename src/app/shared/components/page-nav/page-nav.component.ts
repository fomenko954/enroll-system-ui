import { Component, EventEmitter, Input, OnInit, Output, OnChanges } from '@angular/core';
import { Page } from './page';

@Component({
  selector: 'app-page-nav',
  templateUrl: './page-nav.component.html',
  styleUrls: ['./page-nav.component.scss']
})
export class PageNavComponent implements OnInit, OnChanges {

  @Input() totalPage: number;
  @Input() currentPage: number;
  @Input() size: number;
  @Input() postsPerPage: number[] = [];

  @Output() changePage = new EventEmitter();

  pageModel: Page = {
    page: this.currentPage,
    size: this.size
  };

  pages: any[] = [];

  constructor() {
  }

  ngOnInit(): void {
    if (this.totalPage) {
      this.createPages();
    }
  }

  setPage(page: number, size: number) {
    this.pageModel.page = page;
    this.pageModel.size = size;
    this.changePage.emit(this.pageModel);
  }

  ngOnChanges(): void {
    this.pages = [];
    if (this.totalPage) {
      this.createPages();
    }
  }

  createPages() {
    for (let i = 1; i <= this.totalPage; i++) {
      this.pages.push(i);
    }
  }

  print(value: any) {
    console.log(value);
  }

}
