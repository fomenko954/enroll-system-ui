import { Component, Input, OnInit } from '@angular/core';
import { navigation } from '../../../admin/_nav';
import { UserViewModel } from '../../viewmodel/user-view-model';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() navigation: any[];
  @Input() userInfo: UserViewModel;
  @Input() newUsers: number = 0;

  constructor(private api: ApiService) {
  }

  ngOnInit(): void {
    console.log(navigation);
    console.log(typeof this.userInfo);
  }
}

