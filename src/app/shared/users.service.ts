import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable, ReplaySubject } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { ResponseViewModel } from './viewmodel/response-view-model';
import { UserViewModel } from './viewmodel/user-view-model';
import { SingleResponseViewModel } from './viewmodel/single-response-view-model';
import { FacultyViewModel } from './viewmodel/faculty-view-model';
import { ComplaintsViewModel } from './viewmodel/complaints-view-model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private apiService: ApiService) {
  }

  getUsers(page: number, size: number, fullName?: string): Observable<ResponseViewModel<UserViewModel[]>> {
    let pageRequest = new HttpParams();
    pageRequest = pageRequest.append('page', page.toString());
    pageRequest = pageRequest.append('size', size.toString());

    if (fullName) {
      pageRequest = pageRequest.append('fullNameQ', fullName);
    }

    return this.apiService.get('/api/users', pageRequest);

  }

  verifyMarks(userId): Observable<ResponseViewModel<UserViewModel>> {
    return this.apiService.put(`/api/users/${userId}/status?verifyStatus=VERIFIED`);
  }

  blockUser(userId: number, isBlock: boolean): Observable<ResponseViewModel<UserViewModel>> {
    const httpParam = new HttpParams()
      .append('isBlock', `${isBlock}`);
    return this.apiService.put(`/api/users/${userId}/block`, null, httpParam);
  }

  newUsersCount(): Observable<ResponseViewModel<number>> {
    return this.apiService.get('/api/users/count');
  }

  getUsersMarks(): Observable<ResponseViewModel<SingleResponseViewModel<any>>> {
    return this.apiService.get('/api/users/marks');
  }

  addUsersMars(marks: Array<any>): Observable<ResponseViewModel<any>> {
    const body = {
      marks
    };
    return this.apiService.post('/api/users/marks', body, false);
  }

  registrateOnFaculty(faculty: FacultyViewModel, user: UserViewModel): Observable<ResponseViewModel<any>> {
    return this.apiService.post('/api/complaints', {facultyId: faculty.id, userId: user.id}, false);
  }

  getUsersComplaints(): Observable<ResponseViewModel<SingleResponseViewModel<any>>> {
    return this.apiService.get('/api/users/complaints');
  }
}

