import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { ResponseViewModel } from './viewmodel/response-view-model';
import { FacultyViewModel } from './viewmodel/faculty-view-model';
import { HttpParams } from '@angular/common/http';
import { FacultyModel } from './model/faculty-model';

@Injectable({
  providedIn: 'root'
})
export class FacultiesService {

  constructor(private apiService: ApiService) {
  }

  getAllFaculties(page: number, size: number): Observable<ResponseViewModel<Array<FacultyViewModel>>> {
    const params = new HttpParams()
      .append('page', page.toString())
      .append('size', size.toString());
    return this.apiService.get('/api/faculties', params);
  }

  deleteFaculty(facultyId: number): Observable<ResponseViewModel<string>> {
    return this.apiService.delete(`/api/faculties/${facultyId}`);
  }

  createFaculty(faculty: FacultyModel): Observable<ResponseViewModel<FacultyViewModel>> {
    return this.apiService.post('/api/faculties', faculty, false);
  }

  updateFaculty(faculty: FacultyModel) {
    // this.apiService.put()
  }

  getAllFacultiesForUser(): Observable<ResponseViewModel<Array<FacultyViewModel>>>  {
    return this.apiService.get('/api/users/faculties');
  }
}
