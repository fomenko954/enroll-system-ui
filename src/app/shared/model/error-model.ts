export interface ErrorModel {
  firstName?: string;
  lastName?: string;
  middleName?: string;
  email?: string;
  password?: string;
  confirmPassword?: string;
  city?: string;
  region?: string;
  schoolNumber?: string;
}
