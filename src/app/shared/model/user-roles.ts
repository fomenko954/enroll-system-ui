export enum UserRoles {
  admin = 'ROLE_ADMINISTRATOR',
  student = 'ROLE_STUDENT'
}
