import { Status } from './status';

export interface ComplaintModel {
  userId: number;
  facultyId: number;
  status: Status;
}
