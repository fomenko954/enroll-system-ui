export interface FacultyModel {
  id?: number;
  name: string;
  fundedPlaceCount: number;
  allPlaceCount: number;
  subjectIds: Array<number>;
}
