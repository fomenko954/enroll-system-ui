import { UserRoles } from './user-roles';

export interface UserAuthResponse {
  firstName: string;
  lastName: string;
  middleName: string;
  role: UserRoles;
  email: string;
}
