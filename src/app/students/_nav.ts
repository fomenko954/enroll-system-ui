export const navigation = [
  {
    name: 'Faculties',
    url: '/students/faculties',
    icon: 'book'
  },
  {
    name: 'Profile',
    url: '/students/profile',
    icon: 'account_box'
  }
];
