import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FacultiesComponent } from './faculties/faculties.component';
import { StudentsComponent } from './students.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { StudentsRoutingModule } from './students-routing.module';
import { AuthService } from '../shared/auth.service';
import { AddMarksComponent } from './add-marks/add-marks.component';
import { ProfileComponent } from './profile/profile.component';


@NgModule({
  declarations: [FacultiesComponent, StudentsComponent, AddMarksComponent, ProfileComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    StudentsRoutingModule
  ],
  providers: [AuthService]
})
export class StudentsModule {
}
