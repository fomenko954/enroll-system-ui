import { AfterContentInit, AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SubjectsViewModel } from '../../shared/viewmodel/subjects-view-model';
import * as M from 'materialize-css';
import { SubjectsService } from '../../shared/subjects.service';

@Component({
  selector: 'app-add-marks',
  templateUrl: './add-marks.component.html',
  styleUrls: ['./add-marks.component.scss']
})
export class AddMarksComponent implements AfterViewInit {

  @Input() subjects: SubjectsViewModel[];
  @Output() changeData = new EventEmitter();
  @Output() submitForm = new EventEmitter();

  marks = [
    {
      subjectId: 1,
      mark: 0
    },
    {
      subjectId: 1,
      mark: 0
    },
    {
      subjectId: 1,
      mark: 0
    }
  ];

  constructor(private subjectsService: SubjectsService) {
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.enableSelect(), 500);
  }

  enableSelect() {
    const elem = document.querySelectorAll('select');
    const instances = M.FormSelect.init(elem, {});
    const formSelect: M.FormSelect[] = [];
    elem.forEach(select => {
      formSelect.push(M.FormSelect.getInstance(select));
    });
    this.marks.forEach((mark, index) => mark.subjectId = formSelect[index].getSelectedValues()[0]);
  }

  setData(event, position: number) {
    const index = position - 1;
    if (event.target.tagName === 'SELECT') {
      this.marks[index].subjectId = event.target.value;
    }
    if (event.target.tagName === 'INPUT') {
      this.marks[index].mark = event.target.value;
    }
    this.changeData.emit(this.marks);
  }

  submitAddedMarks(event) {
    event.preventDefault();
    this.submitForm.emit();
  }
}
