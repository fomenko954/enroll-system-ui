import { Component, Input, OnInit } from '@angular/core';
import { FacultyViewModel } from '../../shared/viewmodel/faculty-view-model';
import { FacultiesService } from '../../shared/faculties.service';
import { UsersService } from '../../shared/users.service';
import { UserViewModel } from '../../shared/viewmodel/user-view-model';
import { AuthService } from '../../shared/auth.service';

@Component({
  selector: 'app-faculties',
  templateUrl: './faculties.component.html',
  styleUrls: ['./faculties.component.scss']
})
export class FacultiesComponent implements OnInit {
  faculties: FacultyViewModel[] = [];
  user: UserViewModel;

  constructor(private facultiesService: FacultiesService, private usersService: UsersService, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.authService.getUser().subscribe((response: UserViewModel) => this.user = response);
    this.getFacultyByUser();
  }

  getFacultyByUser() {
    this.facultiesService.getAllFacultiesForUser()
      .subscribe(response => {
          this.faculties = response.content;
        },
        error => {
          alert('Error while getting users');
        });
  }

  registrateOnFaculty(faculty, user): void {
    if (user.verifyStatus === 'NOT_VERIFIED') {
      alert('User must be verified by administrator');
    } else {
      this.usersService.registrateOnFaculty(faculty, user).subscribe(
        response => window.location.reload(),
        error => alert('Error while registrate user on the faculty')
      );
    }
  }
}
