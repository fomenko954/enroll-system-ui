import { Component, OnInit } from '@angular/core';
import { navigation } from './_nav';
import { AuthService } from '../shared/auth.service';
import { UserViewModel } from '../shared/viewmodel/user-view-model';
import { UsersService } from '../shared/users.service';
import { log } from 'util';
import { SubjectsViewModel } from '../shared/viewmodel/subjects-view-model';
import { SubjectsService } from '../shared/subjects.service';
import { FacultyModel } from '../shared/model/faculty-model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {
  navigation = navigation;
  user: UserViewModel;
  subjects: SubjectsViewModel[] = [];
  isLocked: boolean = true;

  marks = [
    {
      subjectId: 1,
      mark: 0
    },
    {
      subjectId: 1,
      mark: 0
    },
    {
      subjectId: 1,
      mark: 0
    }
  ];

  constructor(private authService: AuthService, private usersService: UsersService, private subjectsService: SubjectsService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.authService.getUser().subscribe((response: UserViewModel) => this.user = response);
    this.usersService.getUsersMarks().subscribe(response => {
      this.isLocked = response.content.data.length === 0;
      if (this.isLocked) {
        this.initSubjects();
      }
    });
  }

  private initSubjects(): void {
    this.subjectsService.getAllSubjects()
      .subscribe(response => {
          this.subjects = response.content;
        },
        error => {
          alert('Error while getting subjects');
        });
  }

  changeData(event) {
    this.marks = event;
  }

  submit() {
    if (this.hasDuplicate(this.marks)) {
      alert('Subjects has duplicates, please choose different subjects');
    } else {
      this.usersService.addUsersMars(this.marks).subscribe(response => {
        window.location.reload();
      }, error => {
        alert('Error while adding marks');
      });
    }

  }

  private hasDuplicate(array: Array<any>): boolean {
    return (new Set(array.map(elem => elem.subjectId))).size !== array.map(elem => elem.subjectId).length;
  }
}
