import { Component, OnInit } from '@angular/core';
import * as M from 'materialize-css';
import { UserViewModel } from '../../shared/viewmodel/user-view-model';
import { AuthService } from '../../shared/auth.service';
import { ComplaintsViewModel } from '../../shared/viewmodel/complaints-view-model';
import { UsersService } from '../../shared/users.service';
import { SingleResponseViewModel } from '../../shared/viewmodel/single-response-view-model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: UserViewModel;
  complaints: SingleResponseViewModel<any>;

  constructor(private authService: AuthService, private usersService: UsersService) {
  }

  ngOnInit(): void {
    this.getComplaints();
    this.initTabs();
    this.getUser();
  }

  getUser() {
    this.authService.getUser().subscribe((response: UserViewModel) => this.user = response);
  }

  initTabs() {
    const elems = document.querySelectorAll('.tabs');
    const instances = M.Tabs.init(elems, {});
  }

  getComplaints(): void {
    this.usersService.getUsersComplaints().subscribe(
      response => {
        this.complaints = response.content;
        console.log(this.complaints);
      },
      error => {
        alert('Error while getting complains');
      }
    );
  }
}
