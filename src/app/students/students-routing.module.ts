import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentsComponent } from './students.component';
import { FacultiesComponent } from './faculties/faculties.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  {
    path: 'students',
    component: StudentsComponent,
    children: [
      {
        path: '',
        children: [
          {
            path: '', redirectTo: 'faculties', pathMatch: 'full'
          },
          {
            path: 'faculties', component: FacultiesComponent
          },
          {
            path: 'profile', component: ProfileComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentsRoutingModule {

}
