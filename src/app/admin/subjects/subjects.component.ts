import { Component, OnInit } from '@angular/core';
import { SubjectsViewModel } from '../../shared/viewmodel/subjects-view-model';
import { SubjectsService } from '../../shared/subjects.service';
import * as M from 'materialize-css';
import { FacultyViewModel } from '../../shared/viewmodel/faculty-view-model';
import { FacultyModel } from '../../shared/model/faculty-model';
import { element } from 'protractor';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.scss']
})
export class SubjectsComponent implements OnInit {

  subjects: SubjectsViewModel[] = [];
  size: number = 20;
  page: number = 1;
  totalPage: number = 0;

  subjectViewModel: SubjectsViewModel = {
    name: ''
  };

  changedPage(event) {
    this.page = event.page;
    this.size = event.size;
    this.getAllSubjects(this.page, this.size);
  }

  constructor(private subjectsService: SubjectsService) {
  }

  ngOnInit(): void {
    setTimeout(() => this.enableSelect(), 100);
    this.getAllSubjects(this.page, this.size);
  }


  getAllSubjects(page: number, size: number) {
    this.subjectsService.getAllSubjects(page, size).subscribe(
      response => {
        this.subjects = response.content;
        this.size = response.size;
        this.page = response.currentPage;
        this.totalPage = response.totalPage;
      },
      error => {
        alert('Error while getting subjects');
      }
    );
  }

  deleteSubject(subject: SubjectsViewModel) {
    if (confirm(`Are You sure you want to delete ${subject.name}`)) {
      this.subjectsService.deleteSubject(subject)
        .subscribe(response => {
            const index = this.subjects.indexOf(subject);
            this.subjects.splice(index, 1);
          },
          error => {
            alert('Error while deleting subject');
          });
    }
  }

  addNewSubject() {
    this.subjectsService.createSubject(this.subjectViewModel)
      .subscribe(response => {
          this.subjects.push(response.content);
          this.subjectViewModel.name = '';
        },
        error => {
          alert('Error while creating faculty');
        });
  }

  updateSubject() {
    this.subjectsService.updateSubject(this.subjectViewModel)
      .subscribe(response => {
          this.subjects.forEach(elem => {
              if (elem.id === response.content.id) {
                elem.name = response.content.name;
              }
            }
          );
          this.subjectViewModel.name = '';
        },
        error => {
          alert('Error while creating faculty');
        });
  }

  changeData(event: SubjectsViewModel) {
    this.subjectViewModel.name = event.name;
  }


  enableSelect() {
    const elems = document.querySelectorAll('select');
    const instances = M.FormSelect.init(elems, {});
  }

  setSubject(subject: SubjectsViewModel) {
    this.subjectViewModel.name = subject.name;
    this.subjectViewModel.id = subject.id;
  }
}
