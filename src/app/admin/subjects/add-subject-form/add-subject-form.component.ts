import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { SubjectsViewModel } from '../../../shared/viewmodel/subjects-view-model';
import * as M from 'materialize-css';


@Component({
  selector: 'app-add-subject-form',
  templateUrl: './add-subject-form.component.html',
  styleUrls: ['./add-subject-form.component.scss']
})
export class AddSubjectFormComponent implements OnInit {

  constructor() {
  }

  @Output() changeData = new EventEmitter();
  @Output() submitForm = new EventEmitter();

  subjectsViewModel: SubjectsViewModel = {
    name: '',
  };

  ngOnInit(): void {
    this.enableModal();
  }

  enableModal() {
    const elems = document.querySelector('#modal3');
    const intstances = M.Modal.init(elems, {});
  }

  submitCreation() {
    this.submitForm.emit();
  }

  setData(event) {
    this.subjectsViewModel = {...this.subjectsViewModel, [event.target.name]: event.target.value};
    this.changeData.emit(this.subjectsViewModel);
  }

}
