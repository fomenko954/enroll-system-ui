import { AfterContentChecked, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as M from 'materialize-css';
import { SubjectsViewModel } from '../../../shared/viewmodel/subjects-view-model';

@Component({
  selector: 'app-update-subject-form',
  templateUrl: './update-subject-form.component.html',
  styleUrls: ['./update-subject-form.component.scss']
})
export class UpdateSubjectFormComponent implements OnInit, AfterContentChecked {

  @Output() changeData = new EventEmitter();
  @Output() submitForm = new EventEmitter();
  @Input() subject: SubjectsViewModel;

  constructor() {
  }

  ngOnInit(): void {
    this.enableModal();
  }

  ngAfterContentChecked(): void {
    M.updateTextFields();
  }

  enableModal() {
    const elems = document.querySelector('#modal4');
    const intstances = M.Modal.init(elems, {});
  }

  submitUpdating() {
    this.submitForm.emit();
  }

  setData(event) {
    this.subject = {...this.subject, [event.target.name]: event.target.value };
    this.changeData.emit(this.subject);
  }
}
