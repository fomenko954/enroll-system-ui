import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { UsersComponent } from './users/users.component';
import { FacultiesComponent } from './faculties/faculties.component';
import { SubjectsComponent } from './subjects/subjects.component';
import { ComplainsComponent } from './complains/complains.component';
import { AdminGuardService } from './admin-guard.service';
import { CanDeactivateGuardService } from '../shared/can-deactivate-guard.service';

const routes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AdminGuardService],
    children: [
      {

        path: '',
        children: [
          {
            path: '', redirectTo: 'users', pathMatch: 'full'
          },
          {
            path: 'faculties', component: FacultiesComponent, canDeactivate: [CanDeactivateGuardService]
          },
          {
            path: 'subjects', component: SubjectsComponent, canDeactivate: [CanDeactivateGuardService]
          },
          {
            path: 'users', component: UsersComponent, canDeactivate: [CanDeactivateGuardService]
          },
          {
            path: 'complaints', component: ComplainsComponent, canDeactivate: [CanDeactivateGuardService]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {

}
