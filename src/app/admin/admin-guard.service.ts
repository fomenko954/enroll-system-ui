import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { UserRoles } from '../shared/model/user-roles';

@Injectable({
  providedIn: 'root'
})
export class AdminGuardService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const canActivate = this.canActivateAdmin();
    if (!canActivate) {
      this.router.navigateByUrl('/forbidden');
      return canActivate;
    }
    return canActivate;
  }

  public canActivateAdmin(): boolean {
    return this.authService.hasRole(UserRoles.admin);
  }
}
