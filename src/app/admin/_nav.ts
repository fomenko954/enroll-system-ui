export const navigation = [
  {
    name: 'Users',
    url: '/admin/users',
    icon: 'account_box'
  },
  {
    name: 'Faculties',
    url: '/admin/faculties',
    icon: 'book'
  },
  {
    name: 'Subjects',
    url: '/admin/subjects',
    icon: 'list'
  },
  {
    name: 'Complaints',
    url: '/admin/complaints',
    icon: 'school'
  }
];
