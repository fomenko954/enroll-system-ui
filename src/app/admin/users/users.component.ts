import { Component, OnInit } from '@angular/core';
import { UserViewModel } from '../../shared/viewmodel/user-view-model';
import { UsersService } from '../../shared/users.service';
import * as M from 'materialize-css';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: UserViewModel[] = [];
  size: number = 20;
  page: number = 1;
  totalPage: number = 0;
  searchValue: string = null;

  constructor(private usersService: UsersService) {
  }

  ngOnInit(): void {
    setTimeout(() => this.enableSelect(), 100);
    this.getUsers(this.page, this.size);
  }

  changedPage(event) {
    this.page = event.page;
    this.size = event.size;
    this.getUsers(this.page, this.size);
  }

  getUsers(page: number, size: number, fullName?: string) {
    this.usersService.getUsers(page, size, fullName).subscribe(
      res => {
        console.log(res);
        this.users = res.content;
        this.size = res.size;
        this.page = res.currentPage;
        this.totalPage = res.totalPage;
      }, error => alert('Error in component'));
  }

  verifyUsersMark(userId: number) {
    this.usersService.verifyMarks(userId).subscribe(result => {
        this.users.forEach((elem) => elem.id === userId ? elem.verifyStatus = result.content.verifyStatus : elem);
      },
      error => alert('Error while verifying marks'));
  }

  findUser() {
    this.usersService.getUsers(this.page, this.size, this.searchValue).subscribe(
      res => {
        console.log(res);
        this.users = res.content;
        this.size = res.size;
        this.page = res.currentPage;
        this.totalPage = res.totalPage;
      }, error => alert('Error in component'));
  }

  blockUser(user: UserViewModel) {
    const index = this.users.indexOf(user);

    this.usersService.blockUser(user.id, !this.users[index].isBlocked).subscribe(result => {
      this.users.forEach(elem => {
        if (elem === user) {
          elem.isBlocked = result.content.isBlocked;
        }
      });
    }, error => alert('Error while blocking user'));
  }

  clearSearch() {
    this.searchValue = null;
    this.findUser();
  }

  enableSelect() {
    const elems = document.querySelectorAll('select');
    const instances = M.FormSelect.init(elems, {});
  }
}
