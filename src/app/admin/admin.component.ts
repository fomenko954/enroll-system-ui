import { Component, Input, OnInit } from '@angular/core';
import { navigation } from './_nav';
import { UserViewModel } from '../shared/viewmodel/user-view-model';
import { AuthService } from '../shared/auth.service';
import { UsersComponent } from './users/users.component';
import { UsersService } from '../shared/users.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  navigation = navigation;
  user: UserViewModel;
  newUsersCount: number;

  constructor(private userService: UsersService,
              private auth: AuthService) {
  }

  ngOnInit(): void {
    this.getNewUsers();
    this.auth.getUser().subscribe((result: UserViewModel) => this.user = result, error => alert('Something went wrong'));
  }

  getNewUsers(): void {
    this.userService.newUsersCount().subscribe(result => {
      this.newUsersCount = result.content;
    }, error => alert('Error while retrieving count'));
  }
}
