import { Component, OnInit } from '@angular/core';
import { ComplaintsViewModel } from '../../shared/viewmodel/complaints-view-model';
import { ComplaintsService } from '../../shared/complaints.service';
import * as M from 'materialize-css';
import { UserViewModel } from '../../shared/viewmodel/user-view-model';
import { Status } from '../../shared/model/status';
import { SingleResponseViewModel } from '../../shared/viewmodel/single-response-view-model';
import { FacultyViewModel } from '../../shared/viewmodel/faculty-view-model';


@Component({
  selector: 'app-complains',
  templateUrl: './complains.component.html',
  styleUrls: ['./complains.component.scss']
})
export class ComplainsComponent implements OnInit {
  complaints: ComplaintsViewModel[] = [];
  size: number = 20;
  page: number = 1;
  totalPage: number = 0;
  status = Status;

  constructor(private complaintsService: ComplaintsService) {
  }

  changedPage(event) {
    this.page = event.page;
    this.size = event.size;
    this.getComplaints(this.page, this.size);
  }

  ngOnInit(): void {
    setTimeout(() => this.enableSelect(), 1000);
    this.getComplaints(this.page, this.size);
  }

  getComplaints(page: number, size: number): void {
    this.complaintsService.getAllComplaints(page, size).subscribe(
      response => {
        this.complaints = response.content;
        this.size = response.size;
        this.page = response.currentPage;
        this.totalPage = response.totalPage;
      },
      error => {
        alert('Error while getting complains');
      }
    );
  }

  changeStatus(complaint: ComplaintsViewModel, status: Status) {
    this.complaintsService.changeStatus(complaint, status).subscribe(
      response => {
        const compliant: SingleResponseViewModel<ComplaintsViewModel> = response.content;
        const result: ComplaintsViewModel = compliant.data[0];
        this.complaints.forEach((elem) => {
          if (elem.user.id === compliant.user.id && elem.faculty.id === result.faculty.id) {
            elem.status = result.status;
            elem.acceptedDate = result.acceptedDate;
          }
        });
      },
      error => {
        alert('Error while changing status');
      }
    );
  }

  enableSelect() {
    const elems = document.querySelectorAll('select');
    const instances = M.FormSelect.init(elems, {});
  }
}
