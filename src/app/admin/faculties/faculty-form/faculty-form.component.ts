import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as M from 'materialize-css';
import { FacultyModel } from '../../../shared/model/faculty-model';
import { SubjectsViewModel } from '../../../shared/viewmodel/subjects-view-model';

@Component({
  selector: 'app-faculty-form',
  templateUrl: './faculty-form.component.html',
  styleUrls: ['./faculty-form.component.scss']
})
export class FacultyFormComponent implements OnInit {

  @Input() subjects: SubjectsViewModel[];
  @Output() changeData = new EventEmitter();
  @Output() submitForm = new EventEmitter();

  formSelect: M.FormSelect;

  facultyModel: FacultyModel = {
    name: '',
    fundedPlaceCount: 0,
    allPlaceCount: 0,
    subjectIds: []
  };

  constructor() {
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.enableSelect();
    }, 1000);
    this.enableModal();

  }

  enableModal() {
    const elems = document.querySelectorAll('.modal');
    const intstances = M.Modal.init(elems, {});
  }

  enableSelect() {
    const elem = document.querySelector('#subjects');
    const instances = M.FormSelect.init(elem, {});
    this.formSelect = M.FormSelect.getInstance(elem);
  }

  submitCreation() {
    this.submitForm.emit();
  }

  setData(event) {
    this.facultyModel = {...this.facultyModel, [event.target.name]: event.target.value, subjectIds: this.formSelect.getSelectedValues()};
    this.changeData.emit(this.facultyModel);
  }
}
