import { AfterContentChecked, AfterContentInit, AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SubjectsViewModel } from '../../../shared/viewmodel/subjects-view-model';
import { FacultyModel } from '../../../shared/model/faculty-model';
import * as M from 'materialize-css';


@Component({
  selector: 'app-update-faculty-form',
  templateUrl: './update-faculty-form.component.html',
  styleUrls: ['./update-faculty-form.component.scss']
})
export class UpdateFacultyFormComponent implements OnInit, AfterContentChecked {
  @Input() subjects: SubjectsViewModel[];
  @Input() faculty: FacultyModel;
  @Output() changeData = new EventEmitter();
  @Output() submitForm = new EventEmitter();

  formSelect: M.FormSelect;

  constructor() {
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.enableSelect();
    }, 1000);
    this.enableModal();
  }

  ngAfterContentChecked(): void {
    M.updateTextFields();
  }

  enableModal() {
    const elems = document.querySelectorAll('.modal');
    const intstances = M.Modal.init(elems, {});
  }

  enableSelect() {
    const elem = document.querySelector('#subjects_update');
    const instances = M.FormSelect.init(elem, {});
    this.formSelect = M.FormSelect.getInstance(elem);
  }

  submitUpdating() {
    this.submitForm.emit();
  }

  setData(event) {
    this.faculty = {...this.faculty, [event.target.name]: event.target.value, subjectIds: this.formSelect.getSelectedValues()};
    this.changeData.emit(this.faculty);
  }
}
