import { Component, OnInit } from '@angular/core';
import { FacultiesService } from '../../shared/faculties.service';
import { FacultyViewModel } from '../../shared/viewmodel/faculty-view-model';
import * as M from 'materialize-css';
import { FacultyModel } from '../../shared/model/faculty-model';
import { SubjectsViewModel } from '../../shared/viewmodel/subjects-view-model';
import { SubjectsService } from '../../shared/subjects.service';

@Component({
  selector: 'app-faculties',
  templateUrl: './faculties.component.html',
  styleUrls: ['./faculties.component.scss']
})
export class FacultiesComponent implements OnInit {
  faculties: FacultyViewModel[] = [];
  size: number = 20;
  page: number = 1;
  totalPage: number = 0;
  subjects: SubjectsViewModel[] = [];

  faculty: FacultyModel = {
    name: '',
    fundedPlaceCount: 0,
    allPlaceCount: 0,
    subjectIds: []
  };

  constructor(private facultiesService: FacultiesService,
              private subjectsService: SubjectsService) {
  }

  ngOnInit(): void {
    this.initSubjects();
    setTimeout(() => this.enableSelect(), 100);
    this.getAllFaculties(this.page, this.size);
  }

  getAllFaculties(page: number, size: number) {
    this.facultiesService.getAllFaculties(page, size)
      .subscribe(response => {
          this.faculties = response.content;
          this.size = response.size;
          this.page = response.currentPage;
          this.totalPage = response.totalPage;
        },
        error => {
          alert('Error while getting faculties');
        });
  }

  deleteFaculty(faculty: FacultyViewModel) {
    if (confirm('Are You sure you want to delete this faculty?')) {
      this.facultiesService.deleteFaculty(faculty.id).subscribe(response => {
          const index = this.faculties.indexOf(faculty);
          this.faculties.splice(index, 1);
        },
        error => {
          alert('Error while deleting faculty');
        });
    }
  }

  addNewFaculty() {
    this.facultiesService.createFaculty(this.faculty)
      .subscribe(response => {
          this.faculties.push(response.content);
          this.clearFaculty();
        },
        error => {
          alert('Error while creating faculty');
        });
  }

  updateFaculty() {
    // this.facultiesService.updateFaculty(this.faculty)
    //   .subscribe(response => {
    //     },
    //     error => alert('Error while updating faculty'));
    console.log('Ok');
  }

  private initSubjects() {
    console.log('Init subjects');
    this.subjectsService.getAllSubjects()
      .subscribe(response => {
          this.subjects = response.content;
        },
        error => {
          alert('Error while getting subjects');
        });
  }

  changedPage(event) {
    this.page = event.page;
    this.size = event.size;
    this.getAllFaculties(this.page, this.size);
  }

  changeData(event: FacultyModel) {
    this.faculty.name = event.name;
    this.faculty.fundedPlaceCount = event.fundedPlaceCount;
    this.faculty.allPlaceCount = event.allPlaceCount;
    this.faculty.subjectIds = event.subjectIds;
  }

  enableSelect() {
    const elems = document.querySelectorAll('select');
    const instances = M.FormSelect.init(elems, {});
  }

  setFaculty(faculty: FacultyViewModel) {
    this.faculty.id = faculty.id;
    this.faculty.name = faculty.name;
    this.faculty.fundedPlaceCount = faculty.fundedPlaceCount;
    this.faculty.allPlaceCount = faculty.allPlaceCount;
    this.faculty.subjectIds = faculty.subjects.map(elem => elem.id);
  }

  clearFaculty() {
    this.faculty = {
      name: '',
      fundedPlaceCount: 0,
      allPlaceCount: 0,
      subjectIds: []
    };
  }
}
