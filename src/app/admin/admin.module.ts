import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { SharedModule } from '../shared/shared.module';
import { AuthService } from '../shared/auth.service';
import { UsersComponent } from './users/users.component';
import { FacultiesComponent } from './faculties/faculties.component';
import { FormsModule } from '@angular/forms';
import { FacultyFormComponent } from './faculties/faculty-form/faculty-form.component';
import { UpdateFacultyFormComponent } from './faculties/update-faculty-form/update-faculty-form.component';
import { SubjectsComponent } from './subjects/subjects.component';
import { AddSubjectFormComponent } from './subjects/add-subject-form/add-subject-form.component';
import { UpdateSubjectFormComponent } from './subjects/update-subject-form/update-subject-form.component';
import { ComplainsComponent } from './complains/complains.component';


@NgModule({
  declarations: [AdminComponent, UsersComponent, FacultiesComponent, FacultyFormComponent, UpdateFacultyFormComponent, SubjectsComponent,
    AddSubjectFormComponent, UpdateSubjectFormComponent, ComplainsComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    FormsModule
  ],
  providers: [AuthService]
})
export class AdminModule {
}
