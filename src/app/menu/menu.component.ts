import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/auth.service';

// @ts-ignore
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {


  constructor(public authService: AuthService) {
  }

  ngOnInit(): void {
  }

  logOut(): void {
    this.authService.logout();
  }
}
