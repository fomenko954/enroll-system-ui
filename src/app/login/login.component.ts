import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { NgForm } from '@angular/forms';
import { AuthViewModel } from '../shared/viewmodel/auth-view-model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private auth: AuthService,
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {
    const values = form.value;

    const payload: AuthViewModel = {
      email: values.email,
      password: values.password
    };

    this.auth.authenticate(payload);
  }
}
