import { Component, OnInit } from '@angular/core';
import { AuthService } from './shared/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'enroll-system-ui';

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    if (window.location.pathname !== '/login') { // getUserInfoFromBE will be called from LoginGuard on login
      this.authService.getUserInfo(); // get user info
    }
  }
}
